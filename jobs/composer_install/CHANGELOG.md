# Changelog
All notable changes to this job will be documented in this file.

## [0.2.0] - 2021-04-20
* Remove keyword `cache:when` for longer backward compatibility

## [0.1.0] - 2021-04-14
* Initial version
