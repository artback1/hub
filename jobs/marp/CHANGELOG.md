# Changelog
All notable changes to this job will be documented in this file.

## [0.1.1] - 2022-04-06
* Change old link in the job code

## [0.1.0] - 2021-10-06
* Initial version