# Changelog
All notable changes to this job will be documented in this file.

## [1.0.0] - 2022-04-14
* Change the default stage into `tests`

## [0.1.1] - 2021-12-27
* Set correct return value for GitLab CI/CD pipeline if job succeeds

## [0.1.0] - 2021-07-23
* Initial version
