# Changelog
All notable changes to this job will be documented in this file.

## [1.0.0] - 2022-04-14
* Change the default stage into `tests`

## [0.1.0] - 2022-04-04
* Initial version
