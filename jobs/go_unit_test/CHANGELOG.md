# Changelog
All notable changes to this job will be documented in this file.

## [1.1.0] - 2022-04-14
* Rename GitLab report from `cobertura` to `coverage_report`
* Update Docker image to `golang:1.18`
* Update dependencies to be compatible with golang:1.18

## [1.0.0] - 2022-04-14
* Change the default stage into `tests`

## [0.1.0] - 2021-04-13
* Initial version
